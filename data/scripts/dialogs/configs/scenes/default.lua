background = { -- background information
}

dialog_box = { -- contains ALL dialog box info (including name box)
  image = {
    position = "center", -- The position the dialog box should display at. (can also be a table containing xy values)
    path = "", -- path to dialog box image (leave empty for no image.)
  },
  text = { -- text inside the dialog box
    line = { -- options for the indvidual lines.
      font = "8_bit", -- a font is required! 8_bit is the default one in new Solarus projects.
    },
    question = { -- question box options
    } 
  }
}
