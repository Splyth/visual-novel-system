local PATH = ...
local TOP_DIR = PATH:match"^.+/" or ""
local VNS_DIR = TOP_DIR:match"^(.+/)[^/]+/" --up one directory

local config_loader = {}
local table_helper = require(TOP_DIR.."table_helpers")

local VNS_CONFIG_PATH = VNS_DIR.."configs/"

-- gets the path to the current lanquages config base directory if language is set.
--
-- Returns String containing path to base translation directory. Or nil.
local function get_translation_path()
  if sol.language.get_language() ~= nil then
    return "languages/"..sol.language.get_language().."/configs/"
  end

  return nil
end

-- attempts to load the table at the given path
--
-- path - A string of the path to the file to load.
--
-- Example
--   load_config("/scripts/configs/scenes/path/to/scene/config.lua")
--     #=> {...}
--
--   load_config("/scripts/configs/scenes/invalid/path/to/scene/config.lua")
--     #=> {}
--
-- Returns table of data Raises error if path not found
local function load_config(path)
  if sol.file.exists(path) then
    local data = package.loaded[path] --use existing data if loaded previously
    if not data then --need to load it for the first time
      data = {}

      local env = setmetatable({}, {__newindex = function(self, key, value)
        --TODO validation of data here
        data[key] = value
      end})

      local chunk = sol.main.load_file(path)
      assert(chunk, "Error loading config file: "..path) --probably a syntax error
      setfenv(chunk, env)
      package.preload[path] = chunk
      chunk(path)
      package.loaded[path] = data --save data for fast loading next time
    end

    return data
  end
end

  -- loads the scene configuration. It does so by first loading the default then layering
  -- the options of the dialog name. Ex. After the default config is loaded. Assume that the
  -- id is "hero.greetings.friends.emily" first it will check the hero folder and apply
  -- any configs it finds there, then greetings, than friends, than emily. If no more specific
  -- folder is found it will stop.
  --
  -- id - String which is the dialog id
  --
  -- Example:
  --   load_scene_config('hero.greeting')
  --     #=> {}
  --
  -- Returns table of data (the table is empty if no data found)
  function config_loader:load_scene_config(id)
    local scene_config = {}

    for _, config_path in ipairs({VNS_CONFIG_PATH, get_translation_path()}) do
      local path = config_path.."scenes/"

      -- load scene defaults if they exist
      table_helper.recursive_merge(scene_config, load_config(path.."default.lua") or {})

      -- iterate down through scene groups and add or override the defaults
      local current_path = ""
      for word in (id .. "."):gmatch("([^%.]+)") do
        current_path = current_path..word.."/"
        local config = load_config(path..current_path.."config.lua") or {}
        table_helper.recursive_merge(scene_config, config)
      end
    end

    -- This error means that you probably messed up your paths.
    assert(next(scene_config) ~= nil, "No Configuration Info Found For Scene: ".. id)

    return scene_config
  end

  -- Load config for character from the VNS_HOME/config/characters directory then from languages/CURRENT_LANGUAGE/configs/character
  -- if they exist.
  --
  -- Example
  --   load_character_config("Cortana")
  --     #=> { background = {...}, dialog_box = {...}, ...}
  --
  -- Returns a table of config information for that character
  local function load_character_config(name)
    local character = {}
    for _, config_path in ipairs({VNS_CONFIG_PATH, get_translation_path()}) do
      local path = config_path.."characters/"
      table_helper.recursive_merge(character, load_config(path.."default.lua") or {})
      table_helper.recursive_merge(character, load_config(path..name:lower()..".lua") or {})
    end

    return character
  end

  -- Loads configs for all passed in characters
  --
  -- characters - Array of character names
  --
  -- Example:
  --   load_chacter_configs(['smith', 'noah', 'alexis'])
  --     => { 'smith' => { 'sprite' => 'default', 'tansition' => 'enter_right', etc } }
  --
  -- Returns a table of character information (sprite, position, transitions, etc.)
  function config_loader:load_character_configs(characters)
    local character_configs = {}

    for _,name in ipairs(characters) do
      if not character_configs[name] then --skip if duplicate entry
        character_configs[name] = load_character_config(name)
      end
    end

    return character_configs
  end

  return config_loader
