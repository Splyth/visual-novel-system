# Background

Handles the background that appears during the dialog

## Contains Objects

- [transition](transition.md)
- [image](image.md)

## Examples

### No background/background default

```lua
background = {
  image = {
    path = "default"
  }
}
```

![Example Default background](screenshots/background/default.png)

### background with image

```lua
background = {
  image = {
    path = "demo/visual_novel_art/backgrounds/starry_night_over_the_rhone.png"
  }
}
```

![Example Static background](screenshots/background/static_background.png)

### background with sprite

```lua
background = {
  image = {
    path = "demo/visual_novel_art/backgrounds/animated_background",
    x_offset = 160,
    y_offset = 237,
    sprite = {
      animation = "blur",
      direction = 0
    }
  }
}
```

![Example Animated background](screenshots/background/animated_background.gif)
