# Line

Not to be confused with [text](text.md). Line deals with config options on
the lines themselves (color, font, etc). Text deals with positioning the lines
inside a dialog box.

## Applicable Objects

- [text](text.md)
- [name_box](name_box.md)

## Contains Objects

- [midline_options](midline_options.md)

## Attributes

|name|default|values|description|
|----|-------|------|-----------|
|speed|fast|slow, medium, fast, instant, Any Positive Intgeger| ###Speed|
|apply_speed_to_spaces|false|boolean|Normally spaces appear instantly. Set to true to have spaces display like every other character|
|image| | |please see [midline_options](midline_options.md)|
|sprite| | |please see [midline_options](midline_options.md)|
|color| | |please see the [color documentation](color.md)|
|font_size| |Any Positive Integer| How big to make the font *vector fonts only|
|font| | |what font to use for the line|

### Speed

Speed is how fast (in miliseconds) each character will display on screen. In
other words how fast the text will scroll on screen. We've provided 4 speeds
you can use (slow, medium,fast,instant). If the player pressess a button while
text is scrolling speed is set to `instant` automatically.

If you'd rather specify your own speed you can do so by simply passing an integer instead of the strings mentioned above.

## Examples

```lua
dialog_box = {
  text = {
    line = {
      color = [255,0,255]
    }
  }
}
```

![Example Dialog With color](screenshots/line/RGB.png)

```lua
dialog_box = {
  text = {
    line = {
      speed = "slow"
    }
  }
}
```

![Example Dialog With Slow Speed](screenshots/line/speed.gif)
