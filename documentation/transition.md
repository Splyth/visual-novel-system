# Transition

Allows you to specify transitions for display objects

Currently only fade and slide are supported

## Applicable Objects

- [dialog_box](dialog_box.md)
- [characters](characters.md)
- [background](background.md)

## Usage

They are specified under `transitions` element under each of the objects config.

Enter transitions are specified under the `enter` element.
Exit transitions are specified under the `exit` element.

## Attributes

|name|default|values|description|
|----|-------|------|-----------|
|type|       |fade_in fade_out slide_in slide_out| what transition to use|
|delay| | any integer| how long to play transition|
|slide_direction| |up down left right| What direction to slide (if using slide transition)|

### Delay

The `delay` attribute is technically the "delay" until the dialog box can start displaying text.
But it's also used to determine how long the transition should take.
If multiple objects specify different delays. Each object will use their delay
to determine their transition speed. But the text will not start until the longest
delay is passed.
e.g. if dialog_box has a delay of 10, background a delay of 30, and characters a delay of 25. The text will not display for 30 milliseconds.

## Examples

### dialog box with exit fade

```lua
dialog_box = {
  transitions = {
    exit = {
      type = "fade_out",
      delay = 70
    }
  }
}
```

![Fade in and Slide](screenshots/transitions/dialog_fade_out.gif)

### background with slide from bottom entry

```lua
background = {
  transitions = {
    enter = {
      type = "slide_in",
      delay = 120,
      slide_direction = "up"
    }
  }
}
```

![Fade in and Slide](screenshots/transitions/background_slide_in.gif)

### character fades in and slide out to the right

Want to point out that this character is from a scene config override. NOT from a base character config.

```lua
characters = {
  silver_knight_5 = {
    transitions = {
      enter = {
        type = "fade_in",
        delay = 70
      },
      exit = {
        type = "slide_out",
        slide_direction = "right",
        delay = 70
      }
    }
  }
}
```

![Fade in and Slide](screenshots/transitions/fade_and_slide.gif)
